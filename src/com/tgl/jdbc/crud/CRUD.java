package com.tgl.jdbc.crud;

import java.sql.Connection;
import java.sql.Statement;
import java.util.Map;

import com.tgl.jdbc.data.Condition.Column;

public class CRUD {

	public enum Method{
		CREATE, READ, UPDATE, DELETE
	}
	
	public void crud(Connection connection, Statement stmt, Method method, Map<Column, String> map) {
		Model model = new Model();
		switch (method) {
		case CREATE:
			System.out.println(model.create(connection, stmt, map));
			break;
		case READ:
			model.read(connection, stmt, map);
			break;
		case UPDATE:
			model.update(connection, stmt, map);
			break;
		case DELETE:
			System.out.println(model.delete(connection, stmt, map));
			break;
		default:
			System.out.println("CRUD選擇如下，請在試一次");
			for (Method m : Method.values()) {
				System.out.print(m + " ");
			}
			break;
		}
	}
}
