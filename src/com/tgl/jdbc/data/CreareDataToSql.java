package com.tgl.jdbc.data;

import java.io.BufferedReader;
import java.io.FileReader;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.Statement;
import com.tgl.jdbc.data.Employee;

public class CreareDataToSql {
	private Employee employee;

	public void createData(Connection connection, Statement stmt) {
		employee = new Employee();
		String line;
		String[] word;
		boolean checkInput;
		try (BufferedReader file = new BufferedReader(new FileReader("D:\\fileiodata.txt"))) {
			while ((line = file.readLine()) != null) {
				word = line.split(" "); //讀進一行資料後，以空格切割
				checkInput = checkHeight(word[0]) && checkWeight(word[1]) && checkEnglishName(word[2]) &&
						checkChineseName(word[3]) && checkPhone(word[4]) && checkEmail(word[5]); //判斷輸入的內容是否有符合基本規則
				//長度不為6的話，代表輸入的資料有缺少
				if (word.length != 6) {
					System.out.printf("input error");
					continue;
				}else if(checkInput) {
					employee.setHeight(Integer.valueOf(word[0]));
					employee.setWeight(Integer.valueOf(word[1]));
					employee.setEnglishName(word[2]);
					employee.setChineseName(word[3]);
					employee.setPhone(Integer.valueOf(word[4]));
					employee.setEmail(word[5]);
					
					//將判斷後正確的資料內容放入sql
					String SQL = "INSERT INTO employee(height, weight, englishName, chineseName, phone, email, bmi) VALUES (?, ?, ?, ?, ?, ?, ?)";
					PreparedStatement ps = connection.prepareStatement(SQL);
					ps.setInt(1, employee.getHeight());
					ps.setInt(2, employee.getWeight());
					ps.setString(3, employee.getEnglishName());
					ps.setString(4, employee.getChineseName());
					ps.setInt(5, employee.getPhone());
					ps.setString(6, employee.getEmail());
					ps.setFloat(7, employee.getBmi());
					ps.executeUpdate();
				}
				else {
					System.out.println("input error");
				}
			}
			file.close();
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	//判斷身高輸入
	public boolean checkHeight(String height) {
		int h;
		try {
			h = Integer.valueOf(height);
			if(h > 0 && h < 250) {
				return true;
			}
		}catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}
	
	//判斷體重的輸入
	public boolean checkWeight(String weight) {
		int w;
		try {
			w = Integer.valueOf(weight);
			if(w > 0 && w < 200) {
				return true;
			}
		}catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}
	
	//判斷英文名字的輸入
	public boolean checkEnglishName(String englishname) {
		if (englishname.matches("[a-zA-Z]+-[a-zA-Z]+")) {
			return true;
		}
		return false;
	}
	
	//判斷中文名字的輸入
	public boolean checkChineseName(String chinesename) {
		if (chinesename.matches("[\\u4E00-\\u9FA5]{2,3}")) {
			return true;
		}
		return false;
	}

	//判斷電話的輸入
	public boolean checkPhone(String phone) {
		int p;
		try {
			p = Integer.valueOf(phone);
			if (p > 999 && p < 10000) {
				return true;
			}
		} catch (NumberFormatException e) {
			return false;
		} catch (NullPointerException e) {
			return false;
		}
		return false;
	}
	
	//判斷信箱的輸入
	public boolean checkEmail(String email) {
		if (email.matches("[a-zA-Z0-9._%+]+@[a-zA-Z0-9.-]+\\.[a-z]{2,6}\\.[a-z]*")) {
			return true;
		}
		return false;
	}
}
