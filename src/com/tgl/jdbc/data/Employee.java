package com.tgl.jdbc.data;

public class Employee {
	
	private int rollId;
	private int height;
	private int weight;
	private String englishName;
	private String chineseName;
	private int phone;
	private String email;
	private float bmi;

	public int getHeight() {
		return height;
	}
	public void setHeight(int height) {
		this.height = height;
		setBmi();
	}

	public int getWeight() {
		return weight;
	}
	public void setWeight(int weight) {
		this.weight = weight;
		setBmi();
	}
	
	public String getEnglishName() {
		return englishName;
	}
	public void setEnglishName(String englishName) {
		this.englishName = englishName;
	}
	
	public String getChineseName() {
		return chineseName;
	}
	public void setChineseName(String chineseName) {
		this.chineseName = chineseName;
	}
	
	public int getPhone() {
		return phone;
	}
	public void setPhone(int phone) {
		this.phone = phone;
	}
	
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	
	public float getBmi() {
		return bmi;
	}
	private void setBmi() {
		this.bmi = (float) (this.getWeight() / ((this.getHeight()*0.01) * (this.getHeight()*0.01)));
	}

	@Override
	public String toString() {
		return "Employee[id:" + rollId + " height:" + height + " weight:" + weight +" english name:" + englishName + " chinese name:" + chineseName + " phone:" + phone + " email:" + email + " BMI:" + bmi + "]";
	}
}
